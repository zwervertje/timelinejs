// Define colors
var text_color = '#333333';
// For every type, a new array
// You can have more arrays than types
var box_color = [['#95ec00', '#80B12C',] , ['#009999', '#006363',], ['#6D0F03', '#E62F17',], ['#330349', '#711599',], ['#6D3803', '#E67F17',],] ;

// Define source
// Prerequisites:
// Sorted by start date
// Make sure that every type has an array in box_color with color schemes (e.g. shades of blue)

timeline_source_json =  [{
		"type": "type1",
		"title": "Title 2",
		"subtitle": "Location 2",
		"start_date": "2003-09-01",
		"end_date": "2008-07-01",
		"summary": "Short summary.",
	}, {
		"type": "type2",
		"title": "Title 3",
		"subtitle": "Location 3",
		"start_date": "2010-01-01",
		"end_date": "2011-10-01",
		"summary": "Long summary: But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?",
	}, {
		"type": "type2",
		"title": "A very long title to see how this is truncated",
		"subtitle": "Location 4",
		"start_date": "2011-01-01",
		"end_date": "2012-02-01",
		"summary": "",
	}, {
		"type": "type3",
		"title": "A moderatly long title",
		"subtitle": "Location 5",
		"start_date": "2011-03-01",
		"end_date": "2011-09-01",
		"summary": "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.",
	}, {
		"type": "type1",
		"title": "Title 1",
		"subtitle": "Location 1",
		"start_date": "2003-01-01",
		"end_date": "2003-01-01",
		"summary": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
	}, {
		"type": "type3",
		"title": "Title 6",
		"subtitle": "Location 6",
		"start_date": "2011-10-26",
		"end_date": "2013-02-01",
		"summary": "Summary",

	}];